import gym
import numpy as np

# 1. Load Environment and Q-table structure
env = gym.make('FrozenLake8x8-v0')
Q = np.zeros([env.observation_space.n, env.action_space.n])
# env.observation.n, env.action_space.n gives number of states and actions in loaded env

# 2. Parameters of Q-learning
eta = 0.628
gma = 0.9
epis = 5000 # amount of episodes
rev_list = []   # rewards per epsiode calculated

# 3. Q-learning Algorithm

for i in range(epis):
    # reset environment
    s = env.reset()
    rAll = 0
    d = False
    j = 0

    # the Q-table learning algorithm
    print("##### START ####")
    while j < 99:
        env.render()
        j += 1
        # choose action from Q-Table
        a = np.argmax(Q[s,:] + np.random.randn(1, env.action_space.n)/(i+1))
        # get new state & reward from environment
        s1, r, d, _ = env.step(a)
        # update Q-Table with new knowledge
        Q[s, a] += eta*(r + gma*np.max(Q[s1,:]) - Q[s, a])
        rAll += r
        s = s1
        if d == True:
            env.close()
            break
    rev_list.append(rAll)
    env.render()
print("Reward Sum on all epsiodes {}".format(sum(rev_list)/epis))
print("Final Values Q-Table")
print(Q)
